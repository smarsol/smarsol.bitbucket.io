var SCORM_TRUE = "true";
var SCORM_FALSE = "false";
var initialized = false;

function printError(errorDescription) {
   document.getElementById("error").innerHTML = errorDescription;
}

function showError() {
    API.GetLastError(function(errorNumber){
        API.GetErrorString(errorNumber, function(errorString) {
            API.GetDiagnostic(errorNumber, function(diagnostic) {
                var errorDescription = "Number: " + errorNumber + "\nDescription: " + errorString + "\nDiagnostic: " + diagnostic;

                printError(errorDescription);
            });
        });
    });
}

function updateStatus() {
    document.getElementById("status").innerHTML = "Initialized: "+initialized;
}

function terminate() {
    window.SCORM.Terminate("", function(restul) {
        if (result == SCORM_FALSE){
            showError();
            return;
        }

        initialized = true;
        updateStatus();
    });
}

function getValue() {
    window.SCORM.GetValue(document.getElementById('getValueKey').value, function(result) {
        document.getElementById('getValueResult').value = result;
    });
}

function setValue() {
    window.SCORM.SetValue(document.getElementById('setValueKey').value, document.getElementById('setValueValue').value, function(result) {
        document.getElementById('setValueResult').value = result;
    });
}

function commit() {
    window.SCORM.Commit("", function(result) {
        document.getElementById("commitResult").value = result;
    })
}

function getLastError() {
    window.SCORM.GetLastError(function(result) {
        document.getElementById("getLastErrorResult").value = result;
    })
}

function getErrorString() {
    window.SCORM.GetErrorString(document.getElementById('getErrorStringKey').value, function(result) {
        document.getElementById("getErrorStringResult").value = result;
    });
}

function getDiagnostic() {
    window.SCORM.GetDiagnostic(document.getElementById('getDiagnosticKey').value, function(result) {
        document.getElementById("getDiagnosticResult");
    })
}